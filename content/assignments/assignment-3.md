---
title: "Assignment 3"
date: 2018-08-25T22:54:38+03:00
draft: false
---
# Assignment 3 <span class="tag is-light assignment-due-date">Due: Monday 15/10/2018 at 1:00PM</span>

Install a PHP web app called [Kanboard](https://github.com/kanboard/kanboard) on a remote VM instance on the cloud provider of your choose (e.g., Azure). You need to do the following:

- Download [the latest release of the app](https://github.com/kanboard/kanboard/releases) (the tar.gz file).
- Extract the tar file using `tar -xvzf /path/to/the/file.tar.gz`.
- Check the [requirements](https://docs.kanboard.org/en/latest/admin_guide/requirements.html)
- Install the appropriate software version of PHP, MariaDB, and the Apache HTTP.
- Follow the installation instructions.
- Access the app using the public IP address of your VM instance.

Submit a PDF file that contains your bash script and screenshots of your system showing the execution of the script and the output. The PDF file should be submitted on Slack in private as a direct message (DM) by the due date.