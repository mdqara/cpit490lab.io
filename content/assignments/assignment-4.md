---
title: "Assignment 4"
date: 2018-08-25T22:54:40+03:00
draft: false
---
# Assignment 4 <span class="tag is-light assignment-due-date">Due: Monday 29/10/2018 at 1:00PM</span>

Install and configure a content management system (CMS) called [Drupal](https://www.drupal.org/) on two VM instances on the cloud provider of your choose (e.g., Azure). The first instance should host the web server, and the second instance should host the database server. After installing Drupal, you should replace the default theme with a new one ([see the instructions here](https://www.drupal.org/docs/7/extend/installing-themes)).

Submit a PDF file that contains your bash script and screenshots of your system showing the execution of the script and the output. The PDF file should be submitted on Slack in private as a direct message (DM) by the due date.