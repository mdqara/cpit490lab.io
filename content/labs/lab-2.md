---
title: "Lab 2"
date: 2018-09-01T16:38:51+03:00
draft: false
---

## Lab 2

> The Linux file system and security model. Manage and organize files from the bash shell prompt. Create backups and mount external storage.

<span class="tag is-info is-medium">Due on Thursday 27/9/2018 at 1:00PM</span>

## Objectives
1. Learn the basics of the filesystem structure and operations.
2. Use command line tools to navigate the filesystem and display the contents of directories in a tree-like format.
3. Use common utilities such as _cp_, _mv_, _mkdir_, _rm_, _rmdir_.
4. Understand the permission-based security model of the filesystem.
5. Performing a backup using _rsync_.
5. Adding and removing additional storage devices.

## Setup
- Obtain an external storage device (e.g., external USB hard disk or flash drive)
- Install the following programs if they are not included on your system by default:
  - __tree__ using `sudo yum install tree`.
  - __rsync__ using `sudo yum install rsync`.

## Notes
- Below is a list of commands you will be using in these lab exercises. Make sure you read the manual of each one using the command `man`.
  - `tree`
  - `rsync`
  - `mkdir`
  - `chown`
  - `chmod`
  - `mount`
  - `umount`
- The term "your home directory" refers to the directory under `/home/<your-user-name>` or the directory that you can navigate to using `cd ~`.
  - Replace \<your-user-name\> with your logged in user name. If you are not sure, run the command `whoami`.
- If you attempt to change the owner of a file or directory and receives "operation not permitted", then prefix the command with sudo (e.g., `sudo chown root somefile.txt`).

## Lab Exercises

1. Getting info
  - Obtain the following results
    - Use the `tree` tool to list the content of the _/home_ directory and its sub-directories in a tree-like format.
    - When would you use _tree_ over _ls_?
2. Create a directory called _code_ under your home directory (_/home/\<your-user-name\>_) with the following structure. **NOTE:** Replace \<your-user-name\> with your logged in user name. If you are not sure, run the command `$ whoami`.
     
    ```bash
code/
└── my-project
        ├── LICENSE.txt
        ├── NOTICE.txt
        ├── README.txt
        └── src
            ├── assembly
            ├── it
            ├── main
            │   ├── filters
            │   ├── java
            │   ├── resources
            │   └── webapp
            ├── site
            └── test
                ├── filters
                ├── java
                └── resources
  ```
    - Find the owner and group owner of the file README.txt?
    - Edit the file _README.txt_ using a text editor and write your name inside it.
    - Use the command `chmod` to change the write permission of the owner, so the owner can't write to the file. Keep the write permission for the group. List the permissions using `ls -l code/my-project/README.txt` to inspect it. Now, edit the file and save it. What happens?
    - Change the write permission of both the owner and group owner, so they can't write to the file. Now, edit the file and save it. What happens?
3. Create a new directory called _project-2_ under _/home/\<your-user-name\>/code_. `mkdir code/project-2`.
   - Copy the content of _code/my-project_ into _code/project-2_ using one command using: `cp -r code/my-project/* code/project-2`.
   - Rename _project-2_ directory into _java-starter_.
   - Remove the directory _code/my-project_ and its content.
4. Use the _rsync_ tool to make a backup (sync the content) of the directory code into _/home/\<your-user-name\>/backup_:
   - Create a new directory _/home/\<your-user-name\>/backup_
   - Run `rsync -r /home/<your-user-name>/code/ /home/<your-user-name>/backup/`
     - Note that there is a trailing slash "/" at the end of the source directory. This will sync the content of the source directory into the target directory.
   - Now, add 100 files under _/home/\<your-user-name\>/code/java-starter/src/main/java/_ using:
     - `touch code/java-starter/src/main/java/file{1..100}.java`
         - What happens to the directory at _/home/\<your-user-name\>/backup/java-starter/src/main/java/_ ? List it?
     - Now run rsync again, `rsync -r /home/<your-user-name>/code/ /home/<your-user-name>/backup/`.
         - What happens to the directory at _/home/\<your-user-name\>/backup/java-starter/src/main/java/_ ? List it?
5. File owner and group:
  - Use the command `ls -l` to find the owner and group owner of the _~/code/java-starter/NOTICE.txt_ file.
  - Use the command `chown` to change the user owner of the file _~/code/java-starter/NOTICE.txt_ into the root user. Now, open the file and edit it? What happens?
  - Use the command `chown` to change the user owner and group of the directory _~/code/java-starter_ into _root_ and group _wheel_.
  - Use the command `chown` to change the user owner and group of the directory _~/code/java-starter_ and its subdirectories into _root_ and group _wheel_.
6. File access permissions:
  - Use the command `ls` to inspect the access permissions of the file _~/code/java-starter/LICENSE.txt_. What can the following user categories do: user owner, group, all others? 
  - Use the command `chmod` to change the permissions of the directory _~/code/java-starter_ and its subdirectories where you grant read and write permission to the owner user, none to group members, and none to others. What happens when you run `ls` on the _~/code/java-starter_ directory ? Why?
  - Undo the previous steps by changing the owner and group into your default user name and group name. Also, change the access permissions of the file _~/code/java-starter/LICENSE.txt_ to the earliest values you listed in the first step.

7. Add an external storage and mount it:
  - Run `ls -lh /dev/sd` (then hit tab)
  - Plug in an external storage device.
  - Find the name of the external storage. Type `ls -lh /dev/sd` (then hit tab twice)
      - You may see the new device as sdb1
  - Create a mounting point `mkdir -p /mnt/my-usb-disk`
  - Mount it using the `mount /dev/<sdb1> /mnt/my-usb-disk` command.
  - Create a directory inside the mounted disk. `mkdir /mnt/my-usb-disk/test/`
  - Unmount it using `umount /mnt/my-usb-disk`. How?

## Submission
Submit your answers with screenshots showing the commands you executed as a PDF file by the due date.
