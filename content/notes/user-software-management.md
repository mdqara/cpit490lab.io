---
title: "User Program Management"
date: 2018-09-13T07:42:26+03:00
draft: false
---

# Week 2: User and Software Management

## Part 1: User Management
There're two types of user accounts: superuser or root user and standard users. The root user is a special user account used for system administration and has all the rights or permissions to make modifications in all modes.

It's advised that you avoid logging into the system as a root user for security reasons and to avoid serious damages caused by mistakes in entering commands. If a task requires root level permissions, then you can use the `sudo` command to run it as a root user. The `sudo` command requires the user who issued it to know the root password. 

### 1.1 Creating a Sudo User
Not every standard user can issue the `sudo` command. To give a standard user a `sudo` access, we need to add the user to the `/etc/sudoers` file. This can be done indirectly by adding the user to the `wheel` group. This can be done by logging as a root user, and using the command `usermod -aG wheel <username>`.

```bash
# login as a root
$ su -
$ usermod -aG wheel khalid
```
You can test that the user has superuser privileges by logging in again and prepending `sudo` to a command that requires superuser privileges. For example, listing the contents of the `/root` directory.

```bash
# login again using su - <username> or by logging out using exit
localhost login: khalid
$ sudo ls /root
```

### 1.2 Creating a new group
The command `groupadd <group-name>` is used to create a new user group.

```bash
$ groupadd staff
```

### 1.3 Listing groups
To list all the groups that a user belongs to, use the command `groups <username>`:

```bash
$ groups khalid
khalid wheel
```

To find all the group names on the system, you can list the `/etc/group` file which contains one line for each group.

```bash
$ cat /etc/group
khalid:x:1000:khalid
wheel:x:10:khalid
....
..
sshd:x:74:
ali:x:10001:
```

### 1.4 Adding an existing user to a group
```
$ usermod -aG staff khalid
```

### 1.5 Listing all users on the system
The `/etc/passwd` file contains one line for each user account. Each line contains fields separated by a colon (:) symbol. The first field is the username.

```bash
$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
...
..
...
khalid:x:1000:1000:khalid:/home/khalid:/bin/bash
```

### 1.6 Adding a new user

To add a user to the system, issue the command `useradd <username>`. This command should be issued as a root user or using `sudo`. Otherwise, you will get a Permission denied error.

```bash
$ sudo useradd ali
```

### 1.7 Changing the password
To change the password of an existing user, use the command `passwd`:

```bash
$ sudo passwd ali
```

---

## Part 2: Software Management
Installing and uninstalling software in Linux can be done in various ways. The simplest way is to use a package manager. A package manager is a tool for installing, updating and removing software packages. CentOS uses the `yum` package manager.

It's important to note that installing software often requires a _root user privilege_, so you need to prepend the `yum` command with `sudo`.

### 2.1 Installing a new software
To install a new package, use `yum install <package_name>`.
For example to install [the GNU nano text editor], use the command:
```bash
$ sudo yum install nano
```

### 2.2 Updating installed packages or software

To download and install all packages, type the command:
```bash
$ sudo yum update
```
To update a specific package or software, use `sudo yum update <package_name>`. Example:
```bash
$ sudo yum update nano
```

If you would like to patch your system and restrict the update to only security-related package updates, use:
```bash
$ sudo yum update --security
```

### 2.3 Removing/Uninstalling a new software
To remove an existing package, use `yum remove <package_name>`.
For example to uninstall [the GNU nano text editor], use the command:
```bash
$ sudo yum remove nano
```

___

## Issues

**Q1:** How to check if I'm connecting to the Internet?

Try the _ping_ utility: `ping 1.1.1.1` and use the command `ip address` to see if you have an ip address.

**Q2:** Why is my system not connecting to the Internet?

Edit the file `/etc/sysconfig/network-scripts/ifcfg-<interface_name>` (replace interface_name with the name of your network interface; e.g., enp0s3) and change the line that has `ONBOOT=no` to `ONBOOT=yes`. You need to restart the Operating System to see the effect.