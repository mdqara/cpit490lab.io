---
title: "Project"
date: 2018-10-23T00:07:13+03:00
draft: false
---

# Presentation Project


**Grade:** 10%

## Description

You will need to form a team of two students to present and demo a product or a tool that may be useful in administering or deploying applications or resources on the cloud. You will need to pick a product or a service and submit it to the instructor for approval by the due date shown above. You will do a 15 minutes presentation and a demo at the end of the course.

## What to submit?
- Project idea: A list of the group members and the product or service that you'll present.
- Presentation slides (as a PDF file).

### Type of services or products
- DevOps tools
- Infrastructure monitoring services
- Developer tools and services
- Migration tools and services
- Serverless tools and services
- Logging services
- Analytics and machine learning services
- Search as a Service


## Due Dates

#### Ideas
<span class="tag is-info is-medium">Due on Tuesday 30/10/2018 at 1:00 PM </span>

#### Presentation
<span class="tag is-info is-medium">Due on Tuesday 20/11/2018 at 1:00 PM </span>